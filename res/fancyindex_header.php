<!DOCTYPE html><html>
   <head>
      <meta http-equiv="content-type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width">
      <link rel="stylesheet" href="/themes/tagblog/css/style.css" />
      <style type="text/css">
      <?php
      // You will need the php-yaml (Debian) package installed 
      //~ $nl='<br/>';
      foreach (glob($_SERVER["DOCUMENT_ROOT"]."/config/*.yml") as $file) {
                $arr=yaml_parse_file($file);
                if (isset($arr['tagblog']['colors'])) {
                    //~ echo $arr['tagblog']['colors'].$nl;
                    foreach (['generated/custom-','custom-','generated/',''] as $path) {
                        $path=$_SERVER["DOCUMENT_ROOT"].'/themes/tagblog/css/'.$path.$arr['tagblog']['colors'].'.css';
                        //~ echo $path.$nl;
                        if (file_exists($path)) {
                            include $path;
                            break;
                        }
                    }
                    break;
                }
            }
      ?>
      body,html {
         padding: .3em;
      }
      h1 {
         font-weight: normal;
         padding: 0;
         margin-left: 1em;
      }
      div.home {
          font-size: 1em;
          transform: rotate(90deg);
          transform-origin: top left;
          margin-left: 1em;
          position: absolute;
      }
      tr:hover {
         background: var(--base00,#f5f0e7)
      }
      th {
         text-align:left;
         font-weight:bold;
      }
      #list {
         width:100%;
      }
      td.link {
          padding:0
      }
      td.link a {
         display: block;
         padding:.4em;
         width:100%;
      }
      </style>
   </head>
   <body><div class=home><a href="/">home</a></div>
      <h1>