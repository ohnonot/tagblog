# TagBlog Theme

This [Pico CMS][p] blog theme is centered around a tag list/cloud - other means of navigation, apart from a manually built menu, have not been considered (although posts can be listed/clouded by authors, too).

It is minimal and privacy respecting - no javascript, no 3rd-party resources, simple yet functional CSS design without animations or rounded corners etc.

It's also responsive, a.k.a. mobile friendly.

Colors can be customised through YAML config files. Some `color-*.yml` files are included.

It was originally based on the [NotePaper][np] theme, but has been largely rewritten to focus on the one aspect that was most important to me. I believe that tagblog runs lighter than the original NotePaper.  
Both the original NotePaper and TagBlog are licensed GPL3.

## Screenshots

![Default theme][ssdef]

#### Color variations

[![tagblog desert][ss2t]][ss2]
[![tagblog solarized][ss1t]][ss1]

#### Mobile

[Mobile 3-in-1][ssm]

## Links

TagBlog can be found in two locations:

<https://notabug.org/ohnonot/tagblog>  
<https://framagit.org/ohnonot/tagblog>

## Install

Change to your Pico installation's themes directory:

~~~sh
$ cd <your pico install>/themes
~~~

Clone this repository:

~~~sh
$ git clone https://<notabug or framagit>.org/ohnonot/tagblog
~~~

You can also download the .zip, but make sure the folder structure remains intact.  
In the end you should have this README.md, all twig templates and subfolders in
`<your pico install>/themes/tagblog/`.

The folder `tagblog/content-base` contains important files without which tagblog cannot function (and also an example article that can be deleted). Copy them into your `content` folder.

Copy `res/tagblog.yml` to your Pico CMS installation's `config` folder:

~~~sh
$ cd <your pico install>/config && cp ../themes/tagblog/res/tagblog.yml .
~~~

Add the following line to `<your pico install>/config/config.yml`:

~~~yaml
theme: tagblog
~~~

## Usage & Configuration

~~~
<your pico install>/config/tagblog.yml
~~~

is fairly straightforward and self-explanatory.

Other configuration happens in your markdown articles' YAML headers or through copying/symlinking files.

### Includes

The `includes` folder contains some `*.twig.example` files. `index.twig` looks for their `*.twig` equivalents to include some customisable building blocks into the page layout.

Start with e.g. this:

~~~sh
cd includes && cp footer.twig.example footer.twig
~~~

Now you can edit `footer.twig` to customise your blog's footer.

Once again, this is done to ensure that the git repository is not affected when you start editing files.
In other words, any file inside that folder ending in `.twig` will be ignored by git.

### Tags and other YAML metadata

The theme is fully centered around tags.

Make sure your markdown articles include a valid `tags:` (case insensitive) line in their YAML headers.
A valid header might look like this:

    ---
    Title: Load bash builtin from file
    Author: ohnonot
    Date: 27.02.2018
    Timeline: 01.01.2018, older dates...
    Description: A sort of subtitle, a short explanation
    Excerpt: If you don't want to use automatic excerpt generation you can write some text here.
    Tags: linux,bash
    Template: post
    ---

Tags are separated by commas.
An article that doesn't have a `Tags:` line (or commented out like `#Tags:`) will not show up in the tag cloud/list, not in search results and not in the list of all posts.
It can still be accessed with its direct page URL.

My blog posts all use the `post` template, and are all thrown in one big folder. They can be grouped in subfolders, but the tagblog theme has no way of representing that (the page URL will represent that though).

In `config/tagblog.yml`, `taglist` and `tagcloud` are different designs to show the same clickable lists of tags.

There are two useful views to sort posts:

- `tags`: list articles containing a given tag. This view is accessible by clicking on a tag, either on the sidebar taglist or in a post header.
- `authors`: list articles written by a given author. This view is accessible by clicking on an author name in a post header.

### Automatic Styling

If this theme finds the configuration `tablog.colors` set to a string it will try to find a corresponding CSS file in its folders
and add its contents to the HTML. Precedence in this order, first found will be applied:

- `{{ theme_url }}/css/generated/custom-{{ colors }}.css`
- `{{ theme_url }}/css/custom-{{ colors }}.css`
- `{{ theme_url }}/css/generated/{{ colors }}.css`
- `{{ theme_url }}/css/{{ colors }}.css`

Currently all the shipped stylesheets in `css/generated` do is declare the [CSS variables][cv] `--base00` through `--base0f` for the `:root`
context. These colors are then applied in `css/style.css`, the main stylesheet.

The idea is that e.g. plugins can also use these colors (e.g. PicoPygments), resulting in a unified look.

#### Base16

As the names of the variables suggest they should adhere to the [Base16 Styling Guidelines][bsg].

### Extra CSS styles

`index.twig` will also add an extra stylesheet if

- it finds a file named `custom.css` inside the `css` folder of the theme.  
  This is meant for CSS overrides that apply to all pages.
- it finds the variable `css_extra:` set to a valid string in the YAML header.  
  This is to be an absolute path from Pico's `base_url`,e.g.:  
  `css_extra: /assets/nested-quotes-are-not-quotes.css` will add this line to `index.twig`'s header:
  
  ~~~html
  <link rel="stylesheet" href="{{ base_url }}/assets/nested-quotes-are-not-quotes.css" type="text/css" />
  ~~~

All in all, CSS is applied in this order, all inside the `<head>` section:

1. As link: `{{ theme_url }}/css/style.css`
1. Inline: applies `colors` colors from config.yml
1. Inline: `{{ theme_url }}/css/custom.css` if it exists
1. As link: `css_extra`, if the page's yaml specifies it

### Feed

You can also add a feed to your site. To do that, in the `feed` dict, set `enabled` to `true`. If you wish that feed entries contain your entire post and not only the first lines, set `content` to `true`.

Only pages that have both Tags and Date [metadata][md] will be added to the feed.

### PicoContact

If you are using the [p01contact PicoCMS plugin][pc] ([my fork][pco]), theme colors will affect the contact page, too.
In addition, I recommend replacing the plugin's own `style.css` with the version provided in this repository (`themes/tagblog/css/PicoContact-style.css`).

### Customising

The git repository ignores _any_ file containing the string `custom` in its name.
This ensures that the git project can remain untouched and you can easily `git pull` if there are updates.

## Disclaimers

You should be familiar with the [concepts of PicoCMS][pd].

For deeper understanding I recommend to get familiar with [Twig templating][tt].

Please open an issue in any of the two repositories if you think you found something that should be fixed, or improved.

[p]: https://github.com/picocms/Pico
[pd]: http://picocms.org/docs/
[ss1]: https://dt.iki.fi/stuff/blog/tagblog/TagBlog1.png
[ss1t]: https://dt.iki.fi/stuff/blog/tagblog/TagBlog1.t.png
[ss2]: https://dt.iki.fi/stuff/blog/tagblog/TagBlog2.png
[ss2t]: https://dt.iki.fi/stuff/blog/tagblog/TagBlog2.t.png
[ssdef]: https://dt.iki.fi/stuff/blog/tagblog/TagBlog-default-light.png
[ssm]: https://dt.iki.fi/stuff/blog/tagblog/tagblog-mobile.png
[mcbtoc]: https://github.com/mcbSolutions/Pico-Plugins/tree/master/mcb_TableOfContent
[tt]: https://twig.symfony.com/doc/1.x/
[np]: https://github.com/mayamcdougall/NotePaper
[pc]: https://github.com/nliautaud/p01contact/wiki/Pico-CMS-plugin
[pco]: https://github.com/ohnonot/p01contact
[cv]: https://developer.mozilla.org/en-US/docs/Web/CSS/var
[bsg]: https://github.com/chriskempson/base16/blob/main/styling.md
[md]: #tags-and-other-yaml-metadata
